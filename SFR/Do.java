public class Do {
	public static void main(String[] args) {
		Dog animal = new Dog();
		animal.shout();
		animal.run();
	}
}

class Animal {
	void shout() {
		System.out.println("animal shout！");
	}
}

class Dog extends Animal {
	void shout() {
		super.shout();
		System.out.println("wangwang……");
	}

	void run() {
		System.out.println("Dog is running");
	}
}